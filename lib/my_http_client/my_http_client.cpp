#include <HTTPClient.h>
#include <Arduino.h>
#include "my_http_client.h"

void openDoor(int DOOR_PIN)
{
  digitalWrite(DOOR_PIN, LOW);
}

void closeDoor(int DOOR_PIN)
{
  digitalWrite(DOOR_PIN, HIGH);
}

void callWebhook(String webhook_url,String code,int DOOR_PIN,String payload)
{
  HTTPClient http;
  http.begin(webhook_url);
  int httpCode = http.POST(payload);
  if (httpCode == HTTP_CODE_OK)
  {
    Serial.println("Open door");
    openDoor(DOOR_PIN);
    delay(2000);
    closeDoor(DOOR_PIN);
  }
  else
  {
    Serial.println("Not authorized");
    closeDoor(DOOR_PIN);
  }
  http.end();
}