#include <WiFi.h>
#include "connect.h"

bool connectWifi(const char *ssid, const char *pass, static_ip_config_t *ip)
{
    if (WiFi.status() == WL_CONNECTED)
    {
        return true;
    }
    Serial.println(ip->local_ip);
    if (!WiFi.config(ip->local_ip, ip->gateway, ip->subnet, ip->dns1, ip->dns2))
    {
        Serial.println("STA Failed to configure");
    }
    WiFi.begin(ssid, pass);
    int maxRetries = 20;
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
        maxRetries--;
        if (maxRetries <= 0)
        {
            Serial.println("WiFi false");
            return false;
        }
    }
    Serial.println(WiFi.localIP());
    Serial.println(WiFi.gatewayIP());
    Serial.println(WiFi.subnetMask());
    Serial.println("WiFi connected");
    return true;
}

void reconnect(const char *ssid, const char *pass, static_ip_config_t *ip)
{
    if (WiFi.status() == WL_CONNECTED)
    {
        WiFi.disconnect();
    }
    if (!WiFi.config(ip->local_ip, ip->gateway, ip->subnet, ip->dns1, ip->dns2))
    {
        Serial.println("STA Failed to configure");
    }
    WiFi.begin(ssid, pass);
    int maxRetries = 10;
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(100);
        Serial.print(".");
        maxRetries--;
        if (maxRetries <= 0)
        {
            Serial.println("Config Fail!");
            return;
        }
    }
    Serial.println(WiFi.localIP());
    Serial.println("Config Success");
}

String IPAddressToString(IPAddress *ip)
{
    String ipString = String(ip[0]);
    for (byte octet = 1; octet < 4; ++octet)
    {
        ipString += '.' + String(ip[octet]);
    }
    return ipString;
}

String getTime(String url,int time_offset,int port)
{
    WiFiUDP ntpUDP;
    NTPClient timeClient(ntpUDP, url.c_str(), time_offset, 60000);
    timeClient.begin(port);
    delay(500);
    timeClient.update();
    Serial.println(timeClient.getFormattedDate());
    return timeClient.getFormattedDate();
}