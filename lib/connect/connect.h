#pragma once
#include <stdbool.h>
#include <WiFiUdp.h>
#include <NTPClient.h>

typedef struct {
    IPAddress local_ip;
    IPAddress gateway;
    IPAddress subnet;
    IPAddress dns1;  
    IPAddress dns2; 
} static_ip_config_t;

String IPAddressToString();

bool connectWifi(const char* ssid,const char* pass,static_ip_config_t *ip);
void reconnect(const char* ssid,const char* pass,static_ip_config_t *ip);
String getTime(String url,int time_offset,int port);