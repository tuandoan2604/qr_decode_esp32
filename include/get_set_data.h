#pragma once

#include <Arduino.h>
#include <ArduinoJson.h>
#include "default_config.h"
#include <CRC32.h>

typedef struct
{
    String time;
    String email;
    int meetid;
    char *result = NULL;
} request_unlock;

void creatConfigTemplate(String &dataOutput, device_config_t *device_config);

void parseUpdateConfigTemplate(uint8_t *dataInput, device_config_t *device_config);

String parseQrDecode(char *payload, device_config_t *dev, int *m_eventid);

String creatRequestBodyUnlock(request_unlock *ru);