#ifndef __DEFAULT_CONFIG_H__
#define __DEFAULT_CONFIG_H__

#include <Arduino.h>

#define USERNAME "admin"
#define PASSWORD "admin"
#define WIFI_SSID "VNPT 2.4G"
#define WIFI_PASSWORD "hoilamgi"
#define DEVICE_IP "192.168.2.10"
#define WEBHOOK_URL "https//your-url:8080/endpoint"
#define SERVER_IP "10.1.18.10"
#define SERVER_PORT 8000
#define XAPIKEY "x-api-key-to-call-server-api"
#define ATTLOG_URI "/api/v1/att/visitors"
#define HEARTBEAT_URI "/api/v1/areas/devices/heartbeats/qr-checker"
#define QR_SECRET_KEY "qr-secret-key"
#define NTPSERVER "asia.pool.ntp.org"
#define NTPPORT 123
#define TIMEOFFSET 25200
#define DOOR_RELAY_PIN 12
#define ENABLE_OLED 0
#define ENABLE_ALERT_LED 0

typedef struct
{
    String username = USERNAME;
    String password = PASSWORD;
    String ssid = WIFI_SSID;
    String wf_password = WIFI_PASSWORD;
    String device_ip = DEVICE_IP;
    String webhool_url = WEBHOOK_URL;
    String xapikey = XAPIKEY;
    String server_ip = SERVER_IP;
    String attlog_uri = ATTLOG_URI;
    String heartbeat_uri = HEARTBEAT_URI;
    String qr_secret_key = QR_SECRET_KEY;
    String ntp_server = NTPSERVER;
    int server_port = SERVER_PORT;
    int ntp_port = NTPPORT;
    int devide_id = 1112143;
    int time_offset = TIMEOFFSET;
    bool showip = true;
} device_config_t;

#endif