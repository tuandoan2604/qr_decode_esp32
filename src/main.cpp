#include <Arduino.h>
#include <WiFi.h>
#include <ESP32QRCodeReader.h>
#include "default_config.h"
#include "my_http_client.h"
#include <ESPAsyncWebServer.h>
#include "connect.h"
#include "get_set_data.h"



AsyncWebServer server(80);
ESP32QRCodeReader reader(CAMERA_MODEL_AI_THINKER);
struct QRCodeData qrCodeData;
device_config_t device_config;

void notFound(AsyncWebServerRequest *request)
{
  request->send(404, "text/plain", "Not found");
}

IPAddress local_ip(192, 168, 1, 10);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);
IPAddress dns1(8, 8, 8, 8);
IPAddress dns2(8, 8, 4, 4);

static_ip_config_t sta_ip = {
    sta_ip.local_ip = local_ip,
    sta_ip.gateway = gateway,
    sta_ip.subnet = subnet,
    sta_ip.dns1 = dns1,
    sta_ip.dns2 = dns2};

void setup()
{
  Serial.begin(115200);
  Serial.println();
  pinMode(DOOR_RELAY_PIN, OUTPUT);
  digitalWrite(DOOR_RELAY_PIN, LOW);
  // Setup Camera
  reader.setup();
  Serial.println("Setup QRCode Reader");
  reader.begin();
  Serial.println("Begin QR Code reader");
  
  // Connect WiFi, Webserver Begin
  bool connected = connectWifi(device_config.ssid.c_str(), device_config.wf_password.c_str(), &sta_ip);
  while (!connected);
  server.on("/config", HTTP_GET, [](AsyncWebServerRequest *request)
            {
              if (!request->authenticate(device_config.username.c_str(),device_config.password.c_str())) // Basic Auth
                return request->requestAuthentication();
              String output;
              creatConfigTemplate(output,&device_config);
              Serial.println(output);
              request->send(200, "application/json", output);
            });
  server.on(
      "/config", HTTP_POST, [](AsyncWebServerRequest *request)
      { request->send(200, "text/plain", "Empty Post Request"); },
      NULL, [](AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total)
      {
        if (!request->authenticate(device_config.username.c_str(),device_config.password.c_str()))
          return request->requestAuthentication();
        Serial.println("POST RECEIVED");
        Serial.println((char *)(data));
        parseUpdateConfigTemplate(data,&device_config);
        request->send(200, "text/plain", "RECEIVED DATA");
      });
  server.begin();
  Serial.println("HTTP Server begin");
  delay(1000);
}

void loop()
{
  if (reader.receiveQrCode(&qrCodeData, 100))
  {
    Serial.println("Found QRCode");
    if (qrCodeData.valid)
    {
      Serial.print("Payload: ");
      Serial.println((const char *)qrCodeData.payload);
      
      //(device_config.webhool_url).toCharArray(url, (device_config.webhool_url).length());
      request_unlock ru;
      ru.email = parseQrDecode((char *)qrCodeData.payload,&device_config,&(ru.meetid));
      ru.time = getTime(device_config.ntp_server,device_config.time_offset,device_config.ntp_port);
      String url = "http://"+device_config.server_ip+":"+device_config.server_port+"/api/v1/att/visitors";
      Serial.println(url);
      String payload = creatRequestBodyUnlock(&ru);
      callWebhook(url, String((const char *)qrCodeData.payload), DOOR_RELAY_PIN,payload);
    }
    else
    {
      Serial.print("Invalid: ");
      Serial.println((const char *)qrCodeData.payload);
    }
  }
  delay(300);
}