#include "get_set_data.h"
#include <stdlib.h>

void creatConfigTemplate(String &dataOutput, device_config_t *device_config)
{
    DynamicJsonDocument doc(512);
    //JsonObject obj = doc.as<JsonObject>();
    doc["deviceId"] = 1112143;
    doc["username"] = device_config->username;
    doc["password"] = device_config->password;
    doc["ssid"] = device_config->ssid;
    doc["wifiPwd"] = device_config->wf_password;
    doc["deviceIP"] = device_config->device_ip;
    doc["showIp"] = device_config->showip;
    doc["serverIp"] = device_config->server_ip;
    doc["serverPort"] = device_config->server_port;
    doc["xApiKey"] = device_config->xapikey;
    doc["attLogUri"] = device_config->attlog_uri;
    doc["heartbeatUri"] = device_config->heartbeat_uri;
    doc["qrSecretKey"] = device_config->qr_secret_key;
    doc["ntpServer"] = device_config->ntp_server;
    doc["ntpPort"] = device_config->ntp_port;
    doc["timeOffset"] = device_config->time_offset;
    serializeJsonPretty(doc, dataOutput);
    Serial.println(dataOutput);
}

void parseUpdateConfigTemplate(uint8_t *dataInput, device_config_t *device_config)
{
    DynamicJsonDocument doc(512);
    DeserializationError error = deserializeJson(doc, dataInput);
    if (error)
    {
        Serial.print(F("deserializeJson() failed: "));
        Serial.println(error.f_str());
        return;
    }
    JsonObject obj = doc.as<JsonObject>();
    if (device_config->devide_id == doc["deviceId"])
    {
        device_config->username = obj["username"].as<String>();
        device_config->password = obj["password"].as<String>();
        device_config->ssid = obj["ssid"].as<String>();
        device_config->wf_password = obj["wifiPwd"].as<String>();
        device_config->device_ip = obj["deviceIP"].as<String>();
        device_config->showip = obj["showIp"];
        device_config->server_ip = obj["serverIp"].as<String>();
        device_config->server_port = obj["serverPort"];
        device_config->xapikey = obj["xApiKey"].as<String>();
        device_config->attlog_uri = obj["attLogUri"].as<String>();
        device_config->heartbeat_uri = obj["heartbeatUri"].as<String>();
        device_config->qr_secret_key = obj["qrSecretKey"].as<String>();
        device_config->ntp_server = obj["ntpServer"].as<String>();
        device_config->ntp_port = obj["ntpPort"];
        device_config->time_offset = obj["timeOffset"];
    }

    else
    {
        Serial.println("Wrong DeviceID");
    }
}

/*
    visitor|visitor@email.com|12345|2021-07-31T13:00:00Z|2021-07-31T15:00:00Z|1625805889|8b75ed5d
    Trong đó:
    role: visitor
    visitorEmail: visitor@email.com
    eventId: 12345
    validFrom: 2021-07-31T13:00:00Z (UTC time)
    validUtil: 2021-07-31T15:00:00Z (UTC time)
    generatedTime: 1625805889 (UTC epoch time - salt)
    signature: 8b75ed5d (CRC-32 for checking valid QR) 
*/

String parseQrDecode(char *payload, device_config_t *dev, int *m_eventid)
{
    Serial.println("Start Split String");
    request_unlock ru;
    char *frag;
    frag = strtok(payload, "|");
    char *visitor = frag;
    frag = strtok(0, "|");
    Serial.println(frag);
    String m_mail = frag;
    frag = strtok(0, "|");
    *m_eventid = atoi(frag);
    // frag = strtok(0, "|");
    // char *validFrom = frag;
    // frag = strtok(0, "|");
    // char *validUtil = frag;
    // frag = strtok(0, "|");
    // char *generatedTime = frag;
    char *byteBuffer = strcat(payload, dev->qr_secret_key.c_str());
    CRC32 crc;
    uint32_t checksum = CRC32::calculate((uint8_t *)byteBuffer, strlen(byteBuffer));
    Serial.println(checksum);
    return m_mail;
}

/*
 "time": "2021-07-31T15:12:31Z", //UTC time: thời gian mở cửa
  "visitorEmail": "visitor@email.com", //lấy từ nội dung decode QR
  "meetingId": 12345, // lấy từ nội dung decode QR
  "result": "ACCESS_GRANTED" or "ACCESS_REJECTED" // ACCESS_GRANTED khi QR valid và thực hiện mở cửa
*/

String creatRequestBodyUnlock(request_unlock *ru)
{
    String dataOutput;
    DynamicJsonDocument doc(128);
    doc["time"] = ru->time;
    doc["visitorEmail"] = ru->email;
    doc["meetingId"] = ru->meetid;
    doc["result"] = ru->result;
    
    serializeJsonPretty(doc, dataOutput);
    Serial.println(dataOutput);
    return dataOutput;
}